# Hackathon-2018
Evento 200% técnico + practic0 + proactivo, donde se deberá desarrollar Solo o Team de 2-3 personas una micro APP o micro servicio usado un API de terceros (idea over idea).


## Tematica:
```
- Idea over Idea (Del papel al codigo)
- Refactoring (Ruby/Python a Node)
```

## Solo se tomaran temas relacionados con:
```
- JavaScript (Preferiblemente ES2016)
- Reactjs, Angular v1.6/4-6, Vuejs
- Node (Express, Hapi, Koa, Micro)
- Ruby (Rails, CUBA, Sinatra)
- MongoDB
- Postgres
- Redis, RabbitMQ, Firebase
- AWS, Google Cloud, Docker
- Testing
- Documentacion: Swagger, otros
```

## Consideraciones
```
- Formato: Semi-presencial o Online
- Fechas: Jueves (semi) y Viernes o Viernes (6-8 horas).
- After o fiesta de cierre
- Snack: yerba, café, galletas, turrones y barritas energéticas, Speed, Red Bull, birras
```

## Premios:
```
- Dinero (Pesos, Dolares, PayPal, Bitcoin)
- Equipamento, hardware o software
- Contrato de trabajo Full time (Oficina o Remoto)
- Contrato por horas Freelance
```